import json

from resourcepool.core import generate_id
from resourcepool.messages import Messages


class ResourceRequest(object):
    result = None
    payload = {}
    err_msg = None
    status = Messages.TASK_PENDING

    def __init__(self, username=None, processor_fn=None, payload=None, restore_dump=None):
        if not restore_dump:
            self.id = generate_id()
            self.username = username
            self.processor_fn_name = processor_fn
            if payload:
                self.payload = payload
        else:
            self.restore(restore_dump)

    def __str__(self):
        return self._str_identifier()

    def _str_identifier(self):
        return "ResourceRequest[id={0}]".format(self.id)

    def dump(self):
        return json.dumps(dict(
            id=self.id,
            username=self.username,
            processor_fn_name=self.processor_fn_name,
            payload=self.payload,
        ))

    def restore(self, dump):
        dump = json.loads(dump)
        self.id = dump['id']
        self.username = dump['username']
        self.processor_fn_name = dump['processor_fn_name']
        self.payload = dump['payload']
