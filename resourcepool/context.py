import fnmatch
import imp
import logging
import os

from resourcepool.messages import Messages

# from resourcepool.core.executor.code_manager import CodeManager

logger = logging.getLogger('Context')


class Context(object):
    # code_manager = CodeManager()
    controllers = {}
    _matches = []

    def __init__(self, controller_dir):
        logger.debug('Initialize')
        # self.username = username
        # verify codesync is in place
        # TODO handle error here
        # err, msg = self.code_manager.verify_codebase(username)

        # adding script location to path
        # sys.path.append(
        #     self.code_manager.codesync_repo_path
        # )

        self._import_controllers(controller_dir)

    def _import_controllers(self, controller_dir):
        for root, dirnames, filenames in os.walk(controller_dir):
            for filename in fnmatch.filter(filenames, '*.py'):
                if filename != '__init__.py':
                    fileprefix = filename.split('.py')[0]
                    # importing all controller modules
                    filepath = os.path.join(root, filename)
                    dirs = root.split('contr/')
                    module_label = '{}.{}'.format(dirs[-1], fileprefix) if len(dirs) > 1 else fileprefix
                    # importing module
                    self.controllers[module_label] = imp.load_source(module_label, filepath)
                    self._matches.append(filepath)

    def execute(self, req):
        # module, processor_fn = self.code_manager.get_module(req)
        # req.result = getattr(module, processor_fn[-1])(**req.payload)
        # req.status = Messages.TASK_SUCCESS
        processor = req.processor_fn_name.split('.')
        module_label = ".".join(processor[:-1])
        fn_name = processor[-1]

        req.result = getattr(self.controllers[module_label], fn_name)(**req.payload)
        req.status = Messages.TASK_SUCCESS
