# -*- coding: utf-8 -*-

class Messages(object):
    WORKER_READY = "WORKER_READY"  # Signals worker is ready
    WORKER_HEARTBEAT = "WORKER_HEARTBEAT"  # Signals worker heartbeat

    TASK_PENDING = "PENDING"
    TASK_INPROGRESS = "IN-PROGRESS"
    TASK_SUCCESS = "SUCCESS"
    TASK_FAIL = "FAIL"
