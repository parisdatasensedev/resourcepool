from resourcepool.core.client import BaseResourceClient


class ThreadResourceClient(BaseResourceClient):
    name = 'ThreadResourceClient[id=$ID$]'

    def send(self, req):
        self._make_request(req)
