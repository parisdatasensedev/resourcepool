import json
from resourcepool.core import generate_id


class ResourceResponse(object):
    def __init__(self, req_id=None, status=None, restore_dump=None):
        if not restore_dump:
            self.id = generate_id()
            self.req_id = req_id
            self.status = status
        else:
            self.restore(restore_dump)

    def __str__(self):
        return "ResourceResponse[id={0}, req_id={1}, status={2}]".format(
            self.id, self.req_id, self.status
        )

    def dump(self):
        return json.dumps(dict(
            id=self.id,
            req_id=self.req_id,
            status=self.status,
            type="ResourceResponse",
        ))

    def restore(self, dump):
        self.id = dump['id']
        self.req_id = dump['req_id']
        self.status = dump['status']


class ResultResponse(object):
    def __init__(self, req_id=None, status=None, result=None, restore_dump=None):
        if not restore_dump:
            self.id = generate_id()
            self.req_id = req_id
            self.status = status
            self.result = result
        else:
            self.restore(restore_dump)

    def __str__(self):
        return "ResultResponse[id={0}, req_id={1}, status={2}]".format(
            self.id, self.req_id, self.status
        )

    def dump(self):
        return json.dumps(dict(
            id=self.id,
            req_id=self.req_id,
            status=self.status,
            result=self.result,
            type="ResultResponse",
        ))

    def restore(self, dump):
        self.id = dump['id']
        self.req_id = dump['req_id']
        self.status = dump['status']
        self.result = dump['result']


def responseLoader(dump):
    dump = json.loads(dump)
    if dump['type'] == 'ResourceResponse':
        return ResourceResponse(restore_dump=dump)
    elif dump['type'] == 'ResultResponse':
        return ResultResponse(restore_dump=dump)
