# -*- coding: utf-8 -*-

import logging
import time

import zmq

from resourcepool import conf
from resourcepool.core import generate_id
from resourcepool.messages import Messages
from resourcepool.request import ResourceRequest
from resourcepool.response import ResultResponse

HEARTBEAT_LIVENESS = int(conf.get_config('core_worker', 'heartbeat_liveness'))
HEARTBEAT_INTERVAL = int(conf.get_config('core_worker', 'heartbeat_interval'))
INTERVAL_INIT = int(conf.get_config('core_worker', 'interval_init'))
INTERVAL_MAX = int(conf.get_config('core_worker', 'interval_max'))
WORKER_LISTEN_ADDRESS = conf.get_config('core_worker', 'listen_address')


def create_worker_socket(context, poller, logger, verbose):
    """Helper function that returns a new configured socket
       connected to the Paranoid Pirate queue"""
    logger.debug('Creating Worker')
    worker = context.socket(zmq.DEALER)  # DEALER
    id_ = generate_id()  # refresh id
    identity = id_
    worker.setsockopt(zmq.IDENTITY, identity)
    poller.register(worker, zmq.POLLIN)
    worker.connect(WORKER_LISTEN_ADDRESS)
    msg = Messages.WORKER_READY
    if verbose:
        logger.debug("Sending: [%s] %s" % ('A', msg))
    worker.send(msg)
    return worker


class BaseExecutor(object):
    name = 'default'
    verbose = False

    def __init__(self):
        self.logger = logging.getLogger(self.name)
        self.logger.info('Initialize')

        self._context = zmq.Context(1)
        self._poller = zmq.Poller()


    def listen(self):
        liveness = HEARTBEAT_LIVENESS
        interval = INTERVAL_INIT
        heartbeat_at = time.time() + HEARTBEAT_INTERVAL
        worker = create_worker_socket(self._context, self._poller, self.logger, self.verbose)
        cycles = 0
        while True:
            socks = dict(self._poller.poll(HEARTBEAT_INTERVAL * 1000))
            # Handle self._worker activity on backend
            if socks.get(worker) == zmq.POLLIN:
                #  Get message
                #  - 3-part envelope + content -> request
                #  - 1-part HEARTBEAT -> heartbeat
                frames = worker.recv_multipart()
                if self.verbose:
                    self.logger.debug("Recieved: [%s] %s" % ('B', frames))
                if not frames:
                    break  # Interrupted

                if len(frames) == 3:
                    req_obj = ResourceRequest(restore_dump=frames[2])
                    if self.verbose:
                        self.logger.debug("Sending: [%s] %s" % ('C', req_obj))

                    # deprecate resource response, otherwise workers losing
                    # context of which client to reply to
                    # reply to client that request well received
                    # frames[2] = pickle.dumps(ResourceResponse(
                    #     req_id=req_obj.id, status=req_obj.status)
                    # )
                    # worker.send_multipart(frames)

                    # distribute work to slaves
                    self.handle_task(req_obj)

                    # # check if task succeeded
                    # if req_obj.status != Messages.TASK_SUCCESS:
                    #     self.logger.error('{} because: {}'.format(req_obj.status, req_obj.err_msg))

                    # return result to client
                    frames[2] = ResultResponse(
                        req_id=req_obj.id, status=None, result=None
                    ).dump()
                    worker.send_multipart(frames)

                    # refresh
                    liveness = HEARTBEAT_LIVENESS
                    # slow down communication for debugging
                    # time.sleep(1)
                    #
                    if self.verbose:
                        cycles += 1
                        if cycles % 1000 == 0:
                            self.logger.debug('Treated Requests: %s' % cycles)
                            # cycles = 0

                elif len(frames) == 1 and frames[0] == Messages.WORKER_HEARTBEAT:
                    # self.logger.debug("I: Queue heartbeat")
                    liveness = HEARTBEAT_LIVENESS
                else:
                    self.logger.error("E: Invalid message: %s" % frames)
                interval = INTERVAL_INIT
            else:
                liveness -= 1
                if liveness == 0:
                    self.logger.warning("W: Heartbeat failure, can't reach queue")
                    self.logger.warning("W: Reconnecting in %0.2fs…" % interval)
                    time.sleep(interval)

                    if interval < INTERVAL_MAX:
                        interval *= 2
                    self._poller.unregister(worker)
                    worker.setsockopt(zmq.LINGER, 0)
                    worker.close()
                    worker = create_worker_socket(self._context, self._poller, self.logger, self.verbose)
                    liveness = HEARTBEAT_LIVENESS
            if time.time() > heartbeat_at:
                heartbeat_at = time.time() + HEARTBEAT_INTERVAL
                msg = Messages.WORKER_HEARTBEAT
                if self.verbose:
                    self.logger.debug("Sending: [%s] %s" % ('D', msg))
                worker.send(msg)

    def handle_task(self, req):
        pass
