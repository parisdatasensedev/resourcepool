import imp
import logging
import os

logger = logging.getLogger('CodeManager')

from resourcepool import conf
from resourcepool.core.error import errorcodes
from resourcepool.messages import Messages

CODESYNC_REPO_PATH = conf.get_config('core', 'codesync_repo_path')

# DEPRECATED
class CodeManager(object):
    codesync_repo_path = CODESYNC_REPO_PATH

    def verify_codebase(self, username):
        if not os.path.exists(self.codesync_repo_path):
            err_msg = errorcodes.DIR_NOT_FOUND_CODESYNC_REPO_ROOT.msg.format(self.codesync_repo_path)
            logger.error(err_msg)
            return True, err_msg
        if not os.path.exists(self.codesync_repo_path + username):
            err_msg = errorcodes.DIR_NOT_FOUND_CODESYNC_REPO.msg.format(self.codesync_repo_path + username)
            logger.error(err_msg)
            return True, err_msg
        return False, None

    def get_module(self, req):
        """
        Gets the module referred to in Request
        :param req: ResourceRequest
        :return: Imported Module
        """
        # splitting processor_fn_name
        # to get filename and function name
        processor_fn = req.processor_fn_name.split('.')

        filepath = self.codesync_repo_path + req.username + '/'
        for i, path_ in enumerate(processor_fn[:-1]):
            filepath += path_
            if i < len(processor_fn[:-1]) - 1:
                filepath += '/'

        filepath += '.py'
        if not os.path.exists(filepath):
            req.status = Messages.TASK_FAIL
            req.err_msg = errorcodes.DIR_NOT_FOUND_CODESYNC_REPO.msg.format(filepath)
            return None, None

        # load module and execute function
        module = imp.load_source('imported_module', filepath)
        return module, processor_fn
