# -*- coding: utf-8 -*-
import logging

import zmq
from resourcepool.core.response import responseLoader, ResourceResponse, ResultResponse

from resourcepool import conf
from resourcepool.core import generate_id


class BaseResourceClient(object):
    name = 'default'
    response = None
    _request_timeout = int(conf.get_config('core_client', 'request_timeout'))
    _request_retries = int(conf.get_config('core_client', 'request_retries'))
    _server_endpoint = conf.get_config('core_client', 'server_endpoint')
    _client = None
    verbose = False

    def __init__(self):
        self.id = generate_id()
        self.name = self.name.replace('$ID$', self.id)
        self.logger = logging.getLogger(self.name)
        self.logger.info('Initialize')
        self._context = zmq.Context(1)
        self._connect_to_server()

    def __del__(self):
        # when terminating context, process doesn't terminate
        # self._context.term()
        pass

    def close(self):
        self.__del__()

    def _connect_to_server(self):
        self._client = self._context.socket(zmq.REQ)
        self._client.connect(self._server_endpoint)
        self._poll = zmq.Poller()
        self._poll.register(self._client, zmq.POLLIN)

    def _make_request(self, req_obj):
        client = self._client
        poll = self._poll
        context = self._context
        retries_left = self._request_retries
        # pickle req object
        request = req_obj.dump()
        if self.verbose:
            self.logger.debug("Sending: [%s] %s" % ('A', req_obj))
        client.send(request)

        expect_reply = True
        while expect_reply:
            socks = dict(poll.poll(self._request_timeout))
            if socks.get(client) == zmq.POLLIN:
                reply = client.recv()
                res_obj = responseLoader(reply)

                # de-pickle response
                if self.verbose:
                    self.logger.debug("Recieved: [%s] %s" % ('B', reply))

                if not reply:
                    break
                # if int(res_obj.req_id) == req_obj.id:
                if isinstance(res_obj, ResourceResponse) and res_obj.req_id == req_obj.id:
                    # self.logger.debug("I: Server replied OK (%s)" % reply)
                    self.response = res_obj
                    retries_left = self._request_retries
                    expect_reply = True  # continue expecting result response
                elif isinstance(res_obj, ResultResponse) and res_obj.req_id == req_obj.id:
                    self.response = res_obj
                    expect_reply = False  # end wait, because have received awaited result
                else:
                    self.logger.error("E: Malformed reply from server: %s" % res_obj)

            else:
                self.logger.warning("W: No response from server, retrying…")
                # Socket is confused. Close and remove it.
                client.setsockopt(zmq.LINGER, 0)
                client.close()
                poll.unregister(client)
                retries_left -= 1
                if retries_left == 0:
                    self.logger.debug("E: Server seems to be offline, abandoning")
                    # TODO handle failure
                    break
                self.logger.warning("I: Reconnecting and resending (%s)" % req_obj)
                # Create new connection
                client = context.socket(zmq.REQ)
                client.connect(self._server_endpoint)
                poll.register(client, zmq.POLLIN)
                if self.verbose:
                    self.logger.debug("Sending: [%s] %s" % ('B', req_obj))
                client.send(request)
        if self.verbose:
            self.logger.debug('end make_request')
