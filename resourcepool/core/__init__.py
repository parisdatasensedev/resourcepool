from random import randint


def generate_id():
    return "%04X-%04X" % (randint(0, 0x10000), randint(0, 0x10000))
