import logging
import time
from collections import OrderedDict

import zmq

from resourcepool import conf
from resourcepool.messages import Messages

HEARTBEAT_LIVENESS = int(conf.get_config('core_worker', 'heartbeat_liveness'))
HEARTBEAT_INTERVAL = int(conf.get_config('core_worker', 'heartbeat_interval'))
CLIENT_ADDRESS = conf.get_config('resourcepool', 'client_address')
WORKER_ADDRESS = conf.get_config('resourcepool', 'worker_address')


class WorkerInstance(object):
    def __init__(self, address):
        self.address = address
        self.expiry = time.time() + HEARTBEAT_INTERVAL * HEARTBEAT_LIVENESS


class WorkerQueue(object):
    def __init__(self, logger):
        self.queue = OrderedDict()
        self.logger = logger

    def ready(self, worker):
        self.queue.pop(worker.address, None)
        self.queue[worker.address] = worker

    def purge(self):
        """Look for & kill expired self._workers."""
        t = time.time()
        expired = []
        for address, worker in self.queue.iteritems():
            if t > worker.expiry:  # Worker expired
                expired.append(address)
        for address in expired:
            self.logger.debug("W: Idle worker expired: %s" % address)
            self.queue.pop(address, None)

    def next(self):
        address, worker = self.queue.popitem(False)
        return address


class BaseResourceQueue(object):
    name = 'default'

    _client_address = CLIENT_ADDRESS
    _worker_address = WORKER_ADDRESS
    verbose = False

    def __init__(self):
        self.logger = logging.getLogger(self.name)
        self.logger.info('Initialize')

        self._context = zmq.Context(1)

        self._frontend = self._context.socket(zmq.ROUTER)  # ROUTER
        self._backend = self._context.socket(zmq.ROUTER)  # ROUTER
        self._frontend.bind(self._client_address)  # For clients
        self._backend.bind(self._worker_address)  # For self._workers

        self._poll_workers = zmq.Poller()
        self._poll_workers.register(self._backend, zmq.POLLIN)

        self._poll_both = zmq.Poller()
        self._poll_both.register(self._frontend, zmq.POLLIN)
        self._poll_both.register(self._backend, zmq.POLLIN)

        self._workers = WorkerQueue(self.logger)

        self._heartbeat_at = time.time() + HEARTBEAT_INTERVAL

    def listen(self):
        cycles = 0
        while True:
            if len(self._workers.queue) > 0:
                poller = self._poll_both
            else:
                poller = self._poll_workers
            socks = dict(poller.poll(HEARTBEAT_INTERVAL * 1000))

            # Handle worker activity on self._backend
            if socks.get(self._backend) == zmq.POLLIN:
                # Use worker address for LRU routing
                frames = self._backend.recv_multipart()
                if self.verbose:
                    self.logger.debug("Recieved: [%s] %s" % ('A', frames))
                if not frames:
                    break

                address = frames[0]
                self._workers.ready(WorkerInstance(address))

                # Validate control message, or return reply to client
                msg = frames[1:]
                if len(msg) == 1:
                    if msg[0] not in (Messages.WORKER_READY, Messages.WORKER_HEARTBEAT):
                        self.logger.error("E: Invalid message from worker: %s" % msg)
                else:
                    if self.verbose:
                        self.logger.debug("Sending: [%s] %s" % ('B', msg))
                    self._frontend.send_multipart(msg)
                    #
                    if self.verbose:
                        cycles += 1
                        if cycles % 1000 == 0:
                            self.logger.debug('Brokered Responses: %s' % cycles)
                            # cycles = 0

                # Send heartbeats to idle self._workers if it's time
                if time.time() >= self._heartbeat_at:
                    for worker in self._workers.queue:
                        msg = [worker, Messages.WORKER_HEARTBEAT]
                        if self.verbose:
                            self.logger.debug("Sending: [%s] %s" % ('C', msg))
                        self._backend.send_multipart(msg)
                    self._heartbeat_at = time.time() + HEARTBEAT_INTERVAL
            if socks.get(self._frontend) == zmq.POLLIN:
                frames = self._frontend.recv_multipart()
                if self.verbose:
                    self.logger.debug("Recieved: [%s] %s" % ('D', frames))
                if not frames:
                    break

                frames.insert(0, self._workers.next())
                if self.verbose:
                    self.logger.debug("Sending: [%s] %s" % ('F', frames))
                self._backend.send_multipart(frames)

            self._workers.purge()
