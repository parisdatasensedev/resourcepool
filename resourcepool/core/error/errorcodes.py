class ErrMessage(object):
    def __init__(self, code, msg):
        self.code = code
        self.msg = 'ErrCode:{} | {}'.format(code, msg)


DIR_NOT_FOUND_CODESYNC_REPO_ROOT = ErrMessage(
    500, "Directory Not Found {}",
)

DIR_NOT_FOUND_CODESYNC_REPO = ErrMessage(
    501, "Directory Not Found {}",
)
