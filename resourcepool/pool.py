from resourcepool.core.resource.pool import BaseResourceQueue


class ResourcePool(BaseResourceQueue):
    name = 'ThreadResourcePool'

    def __init__(self):

        super(ResourcePool, self).__init__()
