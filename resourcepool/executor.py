from resourcepool.core.executor.executor import BaseExecutor


class Executor(BaseExecutor):
    name = 'ThreadExecutor'

    def __init__(self, context):
        super(Executor, self).__init__()
        self.context = context

    def handle_task(self, req):
        self.context.execute(req)
