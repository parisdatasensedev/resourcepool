import os
import logging
import configparser

gvar_config_file = 'uni_resource_pool_conf'

if gvar_config_file not in os.environ:
    INI_FILE = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../conf/conf.ini')
else:
    INI_FILE = os.environ[gvar_config_file]

config = configparser.ConfigParser()
config.read(INI_FILE)


# get config
def get_config(section, option):
    return config.get(section=section, option=option)


# if exists config
def if_exists(section, option):
    try:
        return get_config(section, option)
    except configparser.NoSectionError:
        return False
    except configparser.NoOptionError:
        return False


# logger
fmt = '[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s'
datefmt = '%Y-%m-%d %H:%M:%S'
LOG_LEVEL = get_config('log', 'level')
log_file = get_config('log', 'log_file')
if log_file.strip() == '':
    logging.basicConfig(format=fmt, datefmt=datefmt, level=LOG_LEVEL)
else:
    logging.basicConfig(format=fmt, datefmt=datefmt, level=LOG_LEVEL, filename=log_file)

