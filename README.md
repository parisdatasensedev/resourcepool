#resourcepool

## WorkFlow

- Initiate Resource Pool (with provided configurations)
- Resolve Dependencies (make sure all imports will succeed)
- Use Resource:
    - Resource Client makes a Resource Request
        - Providing:
            - Processor Function
            - Data Payload
    - Resource Pool executors Processor Function
    - Resource Pool responds with Resource Response
